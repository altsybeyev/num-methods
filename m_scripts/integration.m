function itegration

tend=6;

linsolve
%%%%%%%%%%%%%%%%
for k=1:10
dt(k)=15/((2^(k+1)));
[T1, X1] = euler(@Dalkvist, [1; 0], dt(k), 0, tend, 1*(2^(k-1)));
xconv(1,k)=X1(1,end);

% [T2, X2] = leapfrog(@Oscillator1, [1; 0], dt(k), 0, tend, 1*(2^(k-1)));
% xconv(2,k)=X2(1,end);

[T3, X3] =adb2(@Dalkvist, [1; 0],  dt(k), 0, tend, 1*(2^(k-1)));
xconv(2,k)=X3(1,end);

end
close all

% figure;
% plot(T2,X2(1,:),T3,X3(1,:))

% close all
figure;
plot(dt,xconv)

% figure;
% plot(dt,xconv(1,:))

figure;
plot(T,X)
%%%%%%%%%%%%%%%%

tend=4;

for k=1:6
dt(k)=4/((2^(k)));
[T X] = euler(@Dalkvist, [1], dt(k), 0, tend, 10*(2^k));
xconv(1,k)=X(1,end);
end

close all
figure;
plot(dt,xconv(1,:))

% figure;
% plot(dt,xconv(1,:))

figure;
plot(T,X)


end

function [T X] = adb2(f, x0, dt, t0, tend, nout)

X=[];
T=[];
Xcur = x0;
Xcur(:,2)=Xcur(:,1);
Xcur(:,3)=Xcur(:,1);

tcur=t0;
i=0;
while (tcur+dt<=tend)
    
   if(mod(i,nout)==0)
        X  = [X Xcur(:,1)];
        T  = [T tcur];
   end
   
   
    if(i==0)
         Xcur(:,2)=Xcur(:,1)+dt*feval(f, Xcur(:,1));
    else
         Xcur(:,3)=Xcur(:,2)+0.5*dt*(feval(f, Xcur(:,2))*3-feval(f, Xcur(:,1)));
    end 
    Xcur(:,1) = Xcur(:,2);
    Xcur(:,2) = Xcur(:,3);
    tcur = tcur+ dt;
   i=i+1;
end

dt = tend - tcur;
Xcur(:,2)=Xcur(:,2)+dt*feval(f,Xcur(:,2));
tcur = tcur + dt;

X  = [X Xcur(:,2)];
T  = [T tcur];

end
    




function dx=Dalkvist(x)
 dx(1,1)=-x(1,1);
end

function dx=Oscillator(x)
dx(1,1)=x(2,1);
dx(2,1)=-x(1,1);
end

function dx=Oscillator1(x)
dx(1,1)=x(2,1);
dx(2,1)=-x(1,1)+0.1*((x(2,1)));
end