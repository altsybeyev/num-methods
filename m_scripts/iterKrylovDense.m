function t


i=0;
for n=400:1000:6400

    i=i+1;

    A=sprand(n, n, 10/n); 
%     A = hilb(n);
    for l=1:n
        A(l,l)=4;
    end
    b=rand(n, 1);    

    condAr(i)=condest(A);
  
   [L,U] = ilu(sparse(A),struct('type','ilutp','droptol',1e-3));

%     tic
% %     x1=linsolve(full(A),b);
%     toc
    tic
    x2=bicgstab(sparse(A),b,1e-16,150);
    toc
    x3=gmres(sparse(A),b,10,1e-16,150);

%     rr1=b-A*x1;
    rr2=b-A*x2;
    rr3=b-A*x3;

%     r1Ar(i)=norm(rr1)./norm(b);
    r2Ar(i)=norm(rr2)./norm(b);
    r3Ar(i)=norm(rr3)./norm(b);
end
[condAr, IX]=sort(condAr);
% r1Ar = r1Ar(IX);
r2Ar = r2Ar(IX);
r3Ar = r3Ar(IX);

% condAr, r1Ar,condAr, r1Ar,'kd',...
loglog(condAr, r2Ar,condAr, r2Ar,'kd',...
       condAr, r3Ar,condAr, r3Ar,'kd');
end