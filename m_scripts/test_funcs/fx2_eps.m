function F = fx2_eps(x, eps)
F = (x)^2 + unifrnd(-eps, eps);
end