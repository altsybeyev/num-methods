function dx=oscillator(x)
dx(1,1)=x(2,1);
dx(2,1)=-x(1,1);
end