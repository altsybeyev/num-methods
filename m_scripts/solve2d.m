function [ output_args ] = solve2d( input_args )

    X0=[13; 15];
    darg=0.001;
    M=zeros(2,2);
    eps=1e-6;
    r=[];
    iter = 0;
    
while(1)
    f=F(X0);

    r=[r max(abs(f))];

    if r(end)<eps || iter>100
        break;
    end

    dX1 = [X0(1)*darg; 0];
    dX2 = [0; X0(2)*darg];        

    M1(:, 1) = (F(X0 + dX1) - F(X0))/dX1(1);
    M1(:, 2) = (F(X0 + dX2) - F(X0))/dX2(2);

    X0=X0-inv(M1)*f;   
%         X0=X0-inv(M1)*f;    

    iter=iter+1;
end
X0
figure;
plot(1:length(r),r);
r(end)
end

function F = F(xx)
x = xx(1);
y = xx(2);
eps=0.01;
F(1,1) = (x-1).^2+(y-2).^2-30;
F(1,1)=F(1,1)*(1+unifrnd(-eps,eps));
F(2,1) = 2*x-y;
F(2,1)=F(2,1)*(1+unifrnd(-eps,eps));

% F = F + unifrnd(-eps*1,eps*1);
end
function dF = dFx(x, eps)
dF = 2*x-2;
% F = F + unifrnd(-eps*1,eps*1);
end