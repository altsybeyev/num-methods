function  x=SOR_iteration(x,A,b,omega)
    for i=1:length(x)
        s=0;
        for j=1:length(x)
            if(i~=j)
                s=s+A(i,j)*x(j);
            end
        end
        x(i)=(omega)*(b(i)-s)/A(i,i)+(1-omega)*x(i);
    end
end