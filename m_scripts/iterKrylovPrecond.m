function t
i=0;


for n=1000:500:2000

    i=i+1;

    A=sprand(n,n,0.01);
    A=sparse(A);

    tic
    condAr(i)=condest(A);
    toc
    b=rand(n, 1);
    
    tic
    [L,U] = ilu(A,struct('type','ilutp','droptol',1e-5));
    toc

    tic
    x1=linsolve(full(A),b);
    toc
    tic
    x2=gmres(A,b,10,1e-3,50,L,U);
    toc
    tic
    x3=bicgstab(A,b,1e-3,50,L,U);
    toc
    
    r1Ar(i)=slau_sol_norm(A,b,x1);
    r2Ar(i)=slau_sol_norm(A,b,x2);
    r3Ar(i)=slau_sol_norm(A,b,x3);
end
[condAr, IX]=sort(condAr);

r1Ar = r1Ar(IX);
r2Ar = r2Ar(IX);
r3Ar = r3Ar(IX);

figure;
loglog(condAr, r1Ar,condAr, r1Ar,'kd',condAr, r2Ar,condAr, r2Ar,'kd',condAr, r3Ar,condAr, r3Ar,'kd');
end