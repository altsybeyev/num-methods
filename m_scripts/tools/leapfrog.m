function [T X] = leapfrog(f, x0, dt, t0, tend, nout)

X=[];
T=[];
Xcur = x0;
tcur=t0;
i=0;
while (tcur+dt<=tend)
    
   if(mod(i,nout)==0)
        X  = [X Xcur];
        T  = [T tcur];
   end
   tmp=feval(f,Xcur);
   Xcur(1,1)=Xcur(1,1)+dt*tmp(1,1);
   tmp=feval(f,Xcur);
   Xcur(2,1)=Xcur(2,1)+dt*tmp(2,1);

   tcur = tcur+ dt;
   i=i+1;

end

dt = tend - tcur;
tmp=feval(f,Xcur);
Xcur(1,1)=Xcur(1,1)+dt*tmp(1,1);

xtmp = Xcur;
% tmp=feval(f,Xcur);
% xtmp(2,1)=xtmp(2,1)+0.5*dt*tmp(2,1);
tmp=feval(f,xtmp);


Xcur(2,1)=Xcur(2,1)+dt*tmp(2,1);
tcur = tcur + dt;

X  = [X Xcur];
T  = [T tcur];

end

