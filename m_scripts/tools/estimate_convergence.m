function res = estimate_convergence( x1, x2, eps )
if abs(x2) < eps
    res = abs(x1-x2) < eps;
else
   res = abs((x1-x2)/x2) < eps;
end

end
