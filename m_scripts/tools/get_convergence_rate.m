function res = get_convergence_rate( x1, x2, x3, x4 )
    res = log((x1-x2)/(x2-x3))/log((x2-x3)/(x3-x4));
end
