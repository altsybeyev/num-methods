function f


x=[-0.02:0.00001:0.02];

n=50;
xmin=-5.12;
xmax=5.12;

for i=1:n
x0(i,1)=unifrnd(xmin,xmax );
end
maxiter=100;

tic
[x1 fval1] = optGauss(@Rosenbrock, x0, xmax*ones(1,n), xmin*ones(1,n));
% [x fval] = optMMK(@Rastrigin, x0, xmax*ones(1,n), xmin*ones(1,n));
[x2 fval2] = optBall(@Rosenbrock, x0, 0.0);
[x3 fval3] = optBall(@Rosenbrock, x0, 0.4);

plot(1:length(fval1),fval1,1:length(fval2),fval2,1:length(fval3),fval3);


plot(fval2)
toc

% [x fval] = optMMK(@Rastrigin, x0, xmax*ones(1,n), xmin*ones(1,n));
% 
% [x2 fval2] = optBall(@Rastrigin, x0, 0.5);
% for k=1:10
%     beta= (k-1)*0.1;
%     [x fval{k}] = optBall(@Rosenbrock, x0, beta);
%     plot(1:length(fval{k}),fval{k});
% 
% end
plot(1:length(fval),fval,1:length(fval1),fval1,1:length(fval2),fval2);
% [x1 fval1] = optGauss(@Rastrigin, x0, xmax*ones(1,n), xmin*ones(1,n));
toc
% z
fval1(end)
x1(:,end)
% figure;
% plot(1:length(fval1),fval1)
% [x fval] = optBall(@Rastrigin, x0);
fval(end)
x(:,end)
figure;
plot(1:length(fval),fval)
end

function [x fval] = optMMK(f, x0, xmax, xmin)

maxiter=1000;
eps=1e-4;

x=x0;
fval(1) = feval(f, x0);
fmin = fval(1);
xBest = x0;

flag=0;

for i=1:maxiter
    for j=1:length(x0)
        x(j,i+1) = unifrnd(xmin(j), xmax(j));
    end
    fvalL = feval(f, x(:,i+1));
    if(fvalL<fmin)
        flag=1;
        fmin = fvalL;
        xBest = x(:,i+1);
    end
    fval(i+1) = fmin;
    if rem(i,20) == 0 && flag==1
        flag=0;
        for j=1:length(x0)
            if xBest(j)-xmin(j)<xmax(j)-xBest(j)
                xmax(j) = xmax(j) - 0.1*( xmax(j) -  xmin(j));
            else
                xmin(j) = xmin(j) + 0.1*( xmax(j) -  xmin(j));
            end
        end
    end
    
    if(norm(x(:,i+1)-x(:,i))<eps)
        break;
    end
end


end
function [x fval] = optGauss(f, x0, xmax, xmin)
maxiter=300;
eps=1e-4;

x=x0;
fval(1) = feval(f, x0);
beta=0.5;
nDiv=21;
for i=1:maxiter
%     x(:,i+1) = x(:,i);
xtmp = x;
    for j=1:length(x0)
        
%         l = int32(unifrnd(1, length(x0)));
        
       x = opt1d(f, x, xmax, xmin, j, nDiv);
       
%         if (i>2)
%             x(j,i+1)= x(j,i+1)+ beta*(x(j,i)-x(j,i-1));
%         end
%                 x(:,i+1) = opt1dAnnealing(f, x(:,i+1), xmax, xmin, j, 100);
%         tt=[x(j,i) x(j,i+1)];
%         tt = sort(tt);
%         x(j,i+1) = unifrnd(tt(1),tt(2));
    end
  
    if(xtmp==x)
        nDiv = unifrnd(21,31);
    end

    
    fval(i+1) = feval(f, x);
    if abs(fval(i+1))<1e-7
        break;
    end

%     if(norm(fval(i+1)-fval(i))<eps)
%         break;
%     end
end

end

function x = opt1d(f, x0, xmax, xmin, component, ndx)

eps=1e-4;

xSearchComp = xmin(component):(xmax(component)- xmin(component))/ndx:xmax(component);
Xcurrect = x0;

imin=[];
fmin=1e15;
for i=1:length(xSearchComp)
    Xcurrect(component) = xSearchComp(i);
    fCurrent = feval(f, Xcurrect);
    if(fCurrent<fmin)
        imin= i;
        fmin = fCurrent;
    end
end

x = x0;
x(component) = xSearchComp(imin);

if(imin==length(xSearchComp))
    xmax(component)=xSearchComp(imin);
else
    xmax(component)=xSearchComp(imin+1);
end

if(imin==1)
    xmin(component)=xSearchComp(imin);
else
    xmin(component)=xSearchComp(imin-1);
end


if(xmax(component) - xmin(component))<eps
    return;
end

x = opt1d(f, x, xmax, xmin, component, ndx);

end


function x = opt1dAnnealing(f, x0, xmax, xmin, component, ndx)

dx = (xmax(component)-xmin(component))/ndx;
iter = 0;
Xcurrect = x0;
while iter<ndx
    Xc = Xcurrect;
    Xc(component) = unifrnd(xmin(component),xmax(component));
%         Xc(component) = unifrnd(Xc(component)-5*dx,Xc(component)+5*dx);

    dE =  feval(f, Xc)-feval(f, Xcurrect);
    if(dE<=0)
        Xcurrect = Xc;
    end
    iter =iter+1;
end

x = Xcurrect;

if(ndx==50)
    return;
end

xmax(component)=Xcurrect(component) + dx;
xmin(component)=Xcurrect(component) - dx;

x = opt1dAnnealing(f, x, xmax, xmin, component, 50);


end





function [x fval] = optGrad(f, x0, varargin)
if size(varargin)
    df = varargin{1};
else
    df = @numGrad;
end
maxiter=1000;
eps=1e-4;
x=x0;
fval(1) = feval(f, x0);
beta =0.1;
for i=1:maxiter
    if size(varargin)
        dfval = feval(df, x(:,i));
    else
        dfval = feval(df, x(:,i), f);
    end
    hi = optStep(f, dfval, x(:,i));
    x(:,i+1) = x(:,i) - hi*dfval ;
    
    fval(i+1) = feval(f, x(:,i+1));

    if(norm(x(:,i+1)-x(:,i))<eps)
        break
    end
end
end


function [x fval] = optBall(f, x0, beta, varargin)
if size(varargin)
    df = varargin{1};
else
    df = @numGrad;
end
maxiter=300;
eps=1e-4;
x=x0;
fval(1) = feval(f, x0);
for i=1:maxiter
    dfval = feval(df, x(:,i), f);
    hi = optStep(f, dfval, x(:,i));
    if(i>2)
        x(:,i+1) = x(:,i) - hi*dfval + beta*(x(:,i)-x(:,i-1));
    else
        x(:,i+1) = x(:,i) - hi*dfval ;
    end
    fval(i+1) = feval(f, x(:,i+1));

    if(norm(x(:,i+1)-x(:,i))<eps)
        break
    end
end
end

function fGrad = numGrad(x, f)

for i=1:length(x)
    h=0.001;
    xh=x;
    xh(i) = xh(i) + h;
    fGrad(i,1) = (feval(f, xh) - feval(f, x))/h;
end
end

function h = optStep(f, dfval, x)
fval=feval(f,x);

h0=0.0001;

x2=feval(f,x - 2*dfval.*h0);

if(x2<fval)
    while (1)
        h0 = h0 * 2; 
        xnew = x - dfval.*h0;
        fvalnew=feval(f,xnew);
        if(fvalnew>fval)
            break;
        end
    end
 
    ha=h0/2;
    hb=h0;
else
    ha=0;
    hb=h0;
end


while (1)
    
    xnewa = x - dfval.*ha;
    xnewb = x - dfval.*hb;
    xnewc = x - dfval.*(hb+ha)/2;

    fvala=feval(f,xnewa);
    fvalb=feval(f,xnewb);

    fvalc=feval(f,xnewc);

    if(fvalb<fvalc)
        ha =(hb+ha)/2;
    else
        hb =(hb+ha)/2;
    end

    if(abs(ha-hb)<1e-5)
        break;
    end
end

h = ha;

end

function f = testx2(x)
    eps=0.1;
    f = (x(1)-3).^2+x(2).^2+x(1).*x(2);
    f = f*unifrnd(1-eps,1+eps);
end
function f = dtestx2(x)
    f(1,1) = 2*(x(1)-3) + x(2);
    f(2,1) = 2*x(2)+ x(1);
end


function f = Rosenbrock(x)
%     eps=0.01;
%     f = 100.0*(x(2)-x(1).^2).^2+(1-x(1)).^2;
%     f = f*unifrnd(1-eps,1+eps);
n=length(x);
f = 0;
    for i=1:n-1
        f = f + (100*(x(i+1)-x(i)^2).^2+ (x(i)-1).^2);
    end
end

function f = RosenbrockGrad(x, varargin)
    f(1,1) = -200*(x(2)-x(1).^2).*2*x(1)  - 2*(1-x(1));
    f(2,1) = 200*(x(2)-x(1).^2) ;
end

function f = Rastrigin(x)
%     eps=0.01;
    A =10;
    n=length(x);
    s=0;
    for i=1:n
        s = s+(x(i).^2-A*cos(2*pi*x(i)));
    end
    f = A*n+s;
%     f = f*unifrnd(1-eps,1+eps);
end




% x0 = [0; 0];
% options = gaoptimset('StallGenLimit',599,'MutationFcn', {@mutationuniform, 0.05}, 'SelectionFcn', @selectionroulette, 'TolFun', 1e-12,'FitnessLimit',0,'CreationFcn', @CreationPopul,'PlotFcns',{@gaplotbestf},'PopulationSize',500);
% options1 = optimoptions('particleswarm','PlotFcn', @bestfval);
% [x,fval] =particleswarm(@Rastrigin,n);
% [x,fval] =ga(@Rastrigin,n, options);

function popul = CreationPopul(GenomeLength, FitnessFcn, options)

for i=1:options.PopulationSize
    for j=1:size(options.PopInitRange,2)
        popul(i,j)=unifrnd(-5.12,5.12);
        
    end
end


end


























function w = w(x)
n=500;
w=0;
a =3;
b = 0.1;
for i=1:n
    w = w+(b^i)*cos((a^n)*pi*x);
end
end